provider "aws" {
  version = "2.12.0"
  region = "eu-west-1"
}
 
resource "aws_instance" "helloworld" {
  ami = "ami-0bb3fad3c0286ebd5"
  instance_type = "t2.micro"
  subnet_id = "subnet-5997f602"
}