variable "namespace" {
  description = "Project namespace name"
  type = string
}
 
variable "ssh_keypair" {
  description = "optional ssh keypair to use for EC2 instance"
  default     = null
  type        = string
}
 
variable "region" {
  description = "AWS region"
  default = "eu-west-1"
  type = string
}