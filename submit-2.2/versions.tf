terraform {
  required_version = "~> 0.13"
  required_providers {
    local    = "~> 1.2"
    aws      = "~> 2.13"
    random   = "~> 2.1"
    template = "~> 2.1"
  }
}